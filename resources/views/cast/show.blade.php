@extends('layout.master')

@section('title')
    Show cast id: {{$cast->id}}
@endsection

@section('content')

<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
            <tr>
                <td>{{$cast->id}}</th>
                <td>{{$cast->nama}}</td>
                <td>{{$cast->umur}}</td>
                <td>{{$cast->bio}}</td>
            </tr>           
    </tbody>
</table>

@endsection