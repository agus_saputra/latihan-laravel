@extends('layout.master')

@section('title')
    Tambah Cast
@endsection

@section('content')
    <div>
        <h2>Tambah Data</h2>
            <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                    <br>
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur"><br>
                    <label for="bio">Bio</label><br>
                    {{-- <input type="text" class="form-control" name="bio" id="umur" placeholder="Masukkan Bio"> --}}
                    <textarea class="form-control" name="bio" id="bio" cols="30" rows="10"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
    </div>
@endsection