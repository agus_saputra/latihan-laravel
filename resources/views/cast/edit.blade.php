@extends('layout.master')

@section('title')
    
    Edit Cast

@endsection

@section('content')
    
<div>
        <h2>Edit cast id: {{$cast->id}}</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <br>
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="umur" placeholder="Masukkan Umur"><br>
                <label for="bio">Bio</label><br>
                {{-- <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="umur" placeholder="Masukkan Umur"> --}}
                <textarea class="form-control" name="bio" id="bio" cols="30" rows="10">{{$cast->bio}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>

@endsection