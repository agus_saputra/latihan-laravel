@extends('layout.master')

@section('title')
    Halaman Form
@endsection

@section('content')
    <form action="/daftar" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <label for="firstname">First name:</label><br><br>
        <input type="text" name="firstname" id="firstname" required><br><br>
        <label for="lastname">Last name:</label><br><br>
        <input type="text" name="lastname" id="lastname" required><br><br>
        <label for="">Gender:</label><br><br>
        <input type="radio" id="male" name="gender" value="male">   
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>
        <label>Nationality :</label><br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporian">Singaporian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label>Language Spoken :</label><br><br>
        <input type="checkbox" name="indonesia" value="Bahasa Indonesia">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="english" value="English">
        <label for="english">English</label><br>
        <input type="checkbox" name="lainnya" value="Other">
        <label for="lainnya">Other</label><br><br>
        <label>Bio :</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Submit">
    </form>
@endsection