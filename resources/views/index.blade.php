@extends('layout.master')

@section('title')
    Halaman Utama
@endsection

@section('content')
    <h1>SanberBook</h1>
    <h2>Social Media Developer Santai Berkualitas</h2>
    <p>Belajar dan berbagi agar hidup ini semakin santai berkualitas</p>
    <h2>Benerfit Join di SanberBook</h2>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastar Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h2>Cara Begabung ke SanberBook</h2>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di <a href="/pendaftaran">Form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>
@endsection