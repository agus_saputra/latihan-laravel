<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DaftarController extends Controller
{
    public function pendaftaran(){
        return view('page.form');
    }

    public function submit(Request $request){
        $namadepan = $request['firstname'];
        $namabelakang = $request['lastname'];
        return view('page.welcome', compact('namadepan', 'namabelakang'));
    }
}
